# Splash window
splash:
  title: Changement en cours
  phrases:
    - En train de faire des choses importantes...
    - Cuisson de Paimon en cours...
    - Invocation en masse pour Yae...
    - Collecte des matériaux...
    - Passage de l'Abysse..
    - Collecte des succès...
    - Équipage de Qiqi qu'en DGT...
    - Préparation de sacrifices pour un 5*...
    - Recherche d'amis pour de la Co-op...

# Launcher window
launcher:
  # Progress bar
  progress:
    pause: Pause
    resume: Continuer

    # Game installation
    game:
      downloading: Téléchargement du jeu...
      unpacking: Décompression du jeu...
      applying_changes: Application des changements...
      deleting_outdated: Suppression des fichiers non à jour...
      integrity_check: Vérification des fichiers...
      download_mismatch_files: Téléchargement des fichiers à corriger...

    # Voice packages installation
    voice:
      deleting: Suppression des pack de voix...
      downloading: 'Téléchargement du pack de voix : {voice}...'
      unpacking: 'Décompression du pack de voix : {voice}...'

  # Launcher states
  states:
    # When the game should be installed or updated
    installation:
      install_wine: Installer Wine
      install_dxvk: Installer DXVK
      install: Installer
      update: Mise à jour
      
      apply_changes:
        # There you may want to do it with a line break "Appliquer" may look better ?
        title: Appliquer les changements
        hint: Appliquer le patch hdiff sur les fichiers du jeu

      remove_outdated:
        # Same as "Appliquer" to keep a consistency if the previous one has a line break this one should else none but welcome long button I guess.
        title: Supprimer les obsolètes
        hint: Supprimer les fichiers du jeu qui sont devenus obsolètes

    # When the game should be patched
    patching:
      # Patch unavailable
      unavailable:
        title: Patch non disponible
        hint: Cette version du jeu n'a pas de patch contre l'anti triche, 
              merci de bien vouloir patienter quelques jours puis réessayez
      
      # Patch is in testing
      test:
        title: Appliquer le patch en teste
        hint: Cette version du jeu a un patch contre l'anti triche, 
              mais celui-ci est en période de teste. 
              Vous pouvez patienter ou bien l'utilisé à vos risques et périls.

      # Patch is stable
      stable: Appliquer le patch

    # When the game is ready for playing
    ready:
      launch: Lancer
      predownload: Pre-téléchargement de mise à jour

# Settings window
settings:
  # General
  general:
    title: Générale
    items:
      # Language selection
      lang:
        # Launcher language
        launcher:
          title: Launcher
          items:
            en-us: English (US)
            ru-ru: Русский
            es-es: Español
            de-de: Deutsch
            fr-fr: Français
            it-it: Italiano
            ja-jp: 日本語
            hu-hu: Magyar
            id-id: Bahasa Indonesia
            nb-no: Norsk
            zh-cn: 简体中文
            uwu: Engwish

        # Game voice pack language
        voice:
          title: Pack de voix
          tooltip: Le choix des voix utilisées sont à modifier 
                   manuellement dans le jeu
          items:
            en-us: Anglaise (US)
            ja-jp: Japonaise
            ko-kr: Coréen
            zh-cn: Chinoise

      # Launcher theme
      theme:
        title: Thème
        items:
          system: Système
          light: Lumineux
          dark: Sombre
      
      # Discord RPC
      discord:
        title: Intégration Discord
        settings:
          title: Intégration Discord paramètres
          items:
            timer: Afficher le temps écouler
            in-launcher: Texte dans le launcher
            in-game: Texte dans le jeu
            selectIcon: Sélection d'icone

      # Some buttons
      buttons:
        winetricks: winetricks
        winecfg: winecfg
        launcher: Dossier du Launcher
        game: Dossier du jeu
        repair_game: Corriger les fichiers du jeu

      # Patch-related settings
      patch:
        title: Patch
        items:
          patch_version: 'Version du patch:'
          updating_info: 'Mise à jour des informations du patch...'
          buttons:
            revert_patch: Désinstaller
            apply_patch: Installer
            reapply_patch: Réinstaller

  # Enhancements
  enhancements:
    title: Améliorations

    # Enhancements related to the wine
    wine:
      title: Wine
      items:
        # HUD
        hud:
          title: Interface
          items:
            none: None
            dxvk: DXVK
            mangohud: MangoHUD

        # Wine synchronization
        winesync:
          title: Synchronisation dans Wine
          tooltip: ESync est un système de synchronisation des opérations multi-thread. Cela améliore vos performances en jeux.
                   FSync est une version améliorée de ESync qui fonctionne seulement avec un Kernel spécifique.
          items:
            none: Aucune
            esync: ESync
            fsync: FSync
            futex2: Futex2

        # AMD FSR
        fsr:
          title: Activer AMD FSR
          tooltip: Cette option active "AMD FidelityFX Super Resolution" (FSR)
                   ce qui permet d'augmenter votre résolution sans perdre des FPS (images par secondes)

        # Wine Virtual Desktop
        winevd:
          title: Bureau virtuel
          settings:
            title: Paramètres du Bureau virtuel
            items:
              width: Largeur
              height: Hauteur

    # Enhancements related to the game
    game:
      title: Jeu
      items:
        # GameMode
        gamemode:
          title: Utiliser GameMode
          tooltip:
            enabled: C'est un logiciel qui permet d'améliorer les performances en jeu
            disabled: ⚠️ Vous n'avez pas le paquet "gamemode" d'installer

        # Borderless Window
        borderless_window:
          title: Fenêtre sans bordures
          tooltip: Enlève les bordures du jeu quand vous jouez en mode fenêtrer.
                   Pour jouer en mode fenêtrer ou inversement jouer en 
                   mode plein écran, appuyez sur alt+entrer pour alterner entre 
                   les deux modes.

        # Unlock FPS
        fps_unlocker:
          title: Débloquer les IPS (FPS)
          tooltip: Cette option permet de débloque le nombre d'image par 
                   secondes (IPS) au-delà de 60

        # Use separate terminal window to run the game
        use_terminal:
            title: Utiliser Terminal
            tooltip: Avec cette option activée le Launcher lancera les commandes Wine dans une fenêtre Terminal séparée

    # Enhancements related to the launcher
    launcher:
      title: Launcher
      items:
        # Delete logs
        purge_logs:
          # Game logs (DXVK)
          game:
            title: Supprimer automatiquement les logs de DXVK
            tooltip: Cette option supprime automatiquement les 
                    fichiers logs de DXVK

          # Launcher logs
          launcher:
            title: Suppression des fichiers logs
            tooltip: Intervalle entre chaque suppression de fichier logs du launcher
            items:
              1d: Tous les jours
              3d: Tous les 3 jours
              5d: Tous les 5 jours
              7d: Toutes les semaines
              14d: Toutes les 2 semaines
              never: Jamais

  # Runners
  runners:
    title: Version de Wine
    items:
      recommended:
        title: Afficher les versions recommandées seulement
        tooltip: Cette option masque les versions de Wine 
                 non testées ou qui ne fonctionnent pas

  # DXVKs
  dxvks:
    title: DXVK
    items:
      recommended:
        title: Afficher les versions recommandées seulement
        tooltip: Cette option masque les versions de DXVK 
                 non testées ou qui ne fonctionnent pas

  # Shaders
  shaders:
    title: Effets Visuels
    items:
      shaders:
        title: Effets Visuels
        tooltip: Utilisez la touche début (home) activer 
                 ou désactiver les effets visuels
        items:
          none: None
          custom: Custom

      author: 'Auteur: {author}'
      no_images: Aucune image ajoutée
      not_installed: Vous n'avez les paquets "vkBasalt" et "reshade-shader library" d'installer

  # Environmantal variables manager
  environment:
    title: Environnement
    items:
      # Table rows
      table:
        name: Nom
        value: Valeur
      
      # Table buttons
      buttons:
        add: Ajouter
        delete: Supprimer

# Notifications
notifications:
  # Launcher update
  launcher_update_available:
    title: 'Mise à jour du Launcher disponible: {from} -> {to}'
    body: Vous pouvez télécharger la nouvelle version du launcher via le dépôt ici {repository}

  # Before telemetry check when iputils is not downloaded
  iputils_package_required:
    title: An Anime Game Launcher
    body: Vous devez installer "iputils" pour pouvoir vérifier la télémétrie

  # When telemetry servers are not disabled
  telemetry_not_disabled:
    title: An Anime Game Launcher
    body: Les serveurs de télémétrie ne sont pas désactivés

  # Before patch applying when xdelta3 package is not downloaded
  xdelta3_package_required:
    title: An Anime Game Launcher
    body: Vous devez télécharger le paquet "xdelta3" pour appliquer le patch

  # If patch wasn't applied because of some error
  patch_applying_error:
    title: An Anime Game Launcher
    body: Le patch n'a pas été appliquer. Vérifiez le fichier log pour en connaitre la raison, ou demandez de l'aide sur le serveur discord.

  # Patch repositories are not available
  patch_repos_unavailable:
    title: An Anime Game Launcher
    body: Pas tous les dépôts des patchs sont disponibles. Vous pouvez lancer le jeu, mais le Launcher ne peut pas garantir que celui-ci est correctement patcher

  # HDiffPatch couldn't successfully apply game files changes
  game_changes_applying_error:
    title: Une erreur a été détectée pendant la mise à jour du jeu
    body: '{files} fichiers n''ont pas pu être mise à jour avec le patch hdiff'

# ToS violation warning window
tos_violation:
  title: Violation du contrat d'utilisation
  heading: ⚠️ Attention
  body: Ce launcher n'est pas un logiciel officiel, il n'est en aucun lier a {company} et/ou {company_alterego}.
        Ce logiciel est conçu pour faciliter l'accès au jeu {game} sur Linux,
        et a été crée seulement pour installer et jouer au jeu sans complication.
        Il fait cela en utilisant les extensions pour facilitée l'utilisation par l'utilisateur.
        En faisant ça certaines extensions utiliser rompt le contrat d'utilisation de {company} pour le jeu {game}.
        Si vous utilisez ce logiciel , votre compte utilisateur peut être identifié par {company}/{company_alterego} comme un compte qui a rompu et/ou non accepter le contrat.
        Si cela arrive, {company}/{company_alterego} a tous les droits sur votre compte ce qui inclut un potentiel Ban.
        Si vous comprenez et acceptez les risques d'utiliser ce logiciel pour jouer appuyez sur OK et partez à l'exploration de Teyvat !
  buttons:
    ok:
      title: Je comprends les risques
      tooltip: Vous devriez réellement lire le texte ci-dessus, c'est important.
    cancel: Annuler
    discord: Notre serveur discord

# Analytics window
analytics:
  title: Commission de Yanfei...
  header: Participer a la collecte des données anonymement

  body:
    - Pour comptabiliser le nombre d'utilisateurs sur Linux, Yanfei aimerait collecter votre adresse IP à chaque mise à jour du jeu
    - Votre adresse IP sera cryptée (méthode Hash) pour des raisons de sécurités

  actions:
    share_country:
      title: Partage du Pays
      hint: Autoriser Yanfei a sauvegardé votre Pays avec votre IP pour rendre les statistiques plus détaillées.
            Aucune autre donnée que votre pays sera sauvegardé

    participate: Participer
    skip: Passer
    skip_forever: Passer et ne plus demander

# Screenshots window
screenshots:
  heading: Captures d'écran
  info: Cliquez sur une capture d'écran pour l'ouvrir
  buttons:
    more: Charger plus
    folder: Ouvrir le dossier des captures d'écran
  no_images: Aucunes captures d'écran prise