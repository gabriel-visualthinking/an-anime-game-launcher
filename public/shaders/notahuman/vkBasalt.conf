# Prime Preset v10 by notahuman
# https://notabug.org/notahuman/presets
# Comparison (v10): https://imgsli.com/MTAyNzc0

## IMPORTANT: ##
# NFAA must be installed separately, it's not included in the crosire/reshade-shaders repository.
# If any of the shaders below do not exist, you will get a blank screen when launching the game.
# vkBasalt does not have fancy error handling, so you will have to launch from a terminal and look for error lines like "vkBasalt err: failed to load shader file".

effects = FakeHDR:AdaptiveSharpen:NFAA:Clarity

# Source: https://github.com/BlueSkyDefender/AstrayFX
NFAA            = /the/absolute/path/to/NFAA.fx
# Source: https://github.com/crosire/reshade-shaders
FakeHDR         = /usr/share/reshade/shaders/FakeHDR.fx
Clarity         = /usr/share/reshade/shaders/Clarity.fx
AdaptiveSharpen = /usr/share/reshade/shaders/AdaptiveSharpen.fx
Tonemap         = /usr/share/reshade/shaders/Tonemap.fx

reshadeTexturePath = "/usr/share/reshade/textures"
reshadeIncludePath = "/usr/share/reshade/shaders"

## FakeHDR ##
# Setting HDRPower higher makes dark things darker.
HDRPower=1.1103111
# The difference between radius1 and radius2 decides the peak brightness.
# Setting radius2 higher than radius1 makes bright things brighter.
radius1=0.9899809
radius2=1.0

## NFAA ##
AA_Adjust=7
Mask_Adjust=0.79

## Clarity ##
ClarityRadius=3
ClarityOffset=0.67
ClarityBlendMode=2
ClarityBlendIfDark=0
ClarityBlendIfLight=113
# If you set this too high, some textures will look flat and washed out, conflicting with FakeHDR.
ClarityStrength=0.0911033
ClarityDarkIntensity=0.41
ClarityLightIntensity=5.0

## AdaptiveSharpen ##
curve_height=0.545553
# Creates a bright outline around dark edges. If set too high, edges can appear jagged; highlighting sharp edges prevents them from blending out.
L_compr_low=0.0
L_compr_high=0.36277
# Darkens dark edges. Charcoal lips if you set this too high
D_compr_low=1.203283
# Darkens edge-adjacent pixels. This resembles cel shading. Shadows, reflections, distant detail, and text appears muddy if you set this too high.
D_compr_high=1.555553

depthCapture = off
toggleKey = Home
